import pixellib
from pixellib.semantic import semantic_segmentation
import cv2
from intel_realsense_camera import *
rs = IntelRealSenseCamera()

#capture = cv2.VideoCapture(0)
ret, capture, depth_color_image, infrared_3d = rs.get_frame_stream()

segment_video = semantic_segmentation()
segment_video.load_pascalvoc_model("deeplabv3_xception_tf_dim_ordering_tf_kernels.h5")
segment_video.process_camera_pascalvoc(capture,  overlay = True, frames_per_second= 15, output_video_name="output_video.mp4", show_frames= True,
frame_name= "frame")
